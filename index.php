<?php
require_once "autoloader.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Task, veebipraktika - php/xml</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="css/myStyle.css">
        <script src="bootstrap/js/bootstrap.js" ></script>
		
    </head>
    <body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      				<h1 class="navbar-brand mb-0">Veebipraktika - andmebaasid</h1>
					<div id="navbarNav">
    					<ul class="navbar-nav">
      						<li class="nav-item active"><a class="nav-link" href="index.php">Task <span class="sr-only">(current)</span></a></li>
      						<li class="nav-item"><a class="nav-link" href="categories.php">Categories</a></li>
							<li class="nav-item"><a class="nav-link" href="actors.php">Actors</a></li>
    					</ul>
  					</div>
    			</nav>
			</div>
		</div>
		<h3 class="alert alert-warning" role="alert">
					Valige 1 ülesanne
		</h3>
		<section class="row">
			<div class="col-12">
				<h1>Ülesanne 1</h1>
				<p>Looge andmebaasi newsDB (valige MySQL, SQLite, PostgreSQL...) ja tabelid news (id, title, description, pubDate, id_category, link), category, user. Ainult registreeritud kasutajad võivad uudised lisada. Uudise lisamisel säilivad andmed andmebaasi ja failisse RSS xml. Lehel peavad kuvastama 10 viimast uudist. Lisaks võib kasutaja vaadata RSS faili formateeritud kujul (XSLT).</p>
				<h1>Ülesanne 2</h1>
				<p>Looge andmebaasi newsDB ja news tabelid (id, title, link uudise portal, ...) ja favorites. Kasutaja võib vaadata uudised lehel ja lisada uudist favorites-sse. Kasutaja võib vaadata uudised favorites-s ja lisada (tabel news) uut uudiste portaali uudisteportaali pärast autenteerimist/registreerimist.</p>
			</div>
		</section>
	</div>
	<footer class="footer">
      		<div class="container">
       		<p class="text-muted">© 2017 Andrei Gavrilov, RDIR51</p>
     		</div>
    	</footer>
    </body>
</html>
